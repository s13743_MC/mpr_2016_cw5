package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrdersService {

//    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
//    	List<Order> ordersMoreThan5 = new ArrayList<Order>();
//    	
//    	for (Order order : orders) {
//    		if (order.getItems().size() > 5) {
//    			ordersMoreThan5.add(order);
//    		}
//    	}
//    	
//    	return ordersMoreThan5;
//    }
	
	//streams
	public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
		
		if (orders == null) {
			return null;
		} else {
			List<Order> ordersMoreThan5streams = orders.stream()
					.filter(order -> order.getItems().size() > 5)
					.collect(Collectors.toList());
			
			return ordersMoreThan5streams;
		}
    }
	
//    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
//    	ClientDetails oldestClient = orders.get(0).getClientDetails();
//    	
//    	for (Order order : orders) {
//    		
//    		if (order.getClientDetails().getAge() > oldestClient.getAge()) {
//    			oldestClient = order.getClientDetails();
//    		}
//    	}
//    	
//    	return oldestClient;
//    }
    
	//streams
    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    	
    	if (orders == null || orders.size() == 0) {
    		return null;
    	} else {
	    	ClientDetails oldestClient = orders.stream()
	    			.map(order -> order.getClientDetails())
	    			.max(Comparator.comparing(ClientDetails::getAge))
	    			.get();
	 
	    	return oldestClient;
    	}
    }
    
//    public static Order findOrderWithLongestComments(List<Order> orders) {
//    	Order orderWithLongestComments = orders.get(0);
//    	
//    	for (Order order : orders) {
//    		
//    		if (order.getComments().length() > orderWithLongestComments.getComments().length()) {
//    			orderWithLongestComments = order;
//    		}
//    	}
//    	
//    	return orderWithLongestComments;
//    }
    
    //streams
    public static Order findOrderWithLongestComments(List<Order> orders) {
    	
    	if (orders == null || orders.size() == 0) {
    		return null;
    	} else {
	    	Order orderWithLongestComments = orders.stream()
	    			.max((x,y) -> x.getComments().length() - y.getComments().length())
	    			.get();
	    			
	    	return orderWithLongestComments;
    	}
    }
    
//    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
//    	String namesAbove18Years = "";
//    	List<ClientDetails> clientsAbove18Years = new ArrayList<ClientDetails>();
//    	
//    	for (Order order : orders) {
//    		ClientDetails client = order.getClientDetails();
//    		if (client.getAge() > 17) {
//    			if (!clientsAbove18Years.contains(client)) {
//    				clientsAbove18Years.add(order.getClientDetails());
//    			}
//    		}
//    	}
//    	
//    	for (ClientDetails client : clientsAbove18Years) {
//    		if((clientsAbove18Years.size()-1) == clientsAbove18Years.lastIndexOf(client)) {
//    			namesAbove18Years = namesAbove18Years + client.getName() + " " + client.getSurname();
//    		}
//    		else {
//    			namesAbove18Years = namesAbove18Years + client.getName() + " " + client.getSurname() + ", ";
//    		}
//    	}
//    	
//    	return namesAbove18Years;
//    }
    
    //streams
    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
    	
    	if (orders == null || orders.size() == 0) {
    		return null;
    	} else {
	    	String namesAbove18Years = orders.stream()
	    			.filter(order -> order.getClientDetails().getAge() > 17)
	    			.map(order -> order.getClientDetails().getName() + " " + order.getClientDetails().getSurname())
	    			.distinct()
	    			.collect(Collectors.joining(", "));
	    	
	    	return namesAbove18Years;
    	}
    }
    
//    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
//    	List<String> ordersItemsNamesfromOrderWithCommentA = new ArrayList<String>();
//    	
//    	for (Order order: orders) {
//    		if (order.getComments().startsWith("A")) {
//    			for (OrderItem orderItem : order.getItems()) {
//    				if(!ordersItemsNamesfromOrderWithCommentA.contains(orderItem.getName())) {
//    					ordersItemsNamesfromOrderWithCommentA.add(orderItem.getName());
//    				}
//    			}
//    		}
//    	}
//    	
//    	Collections.sort(ordersItemsNamesfromOrderWithCommentA);
//    	
//    	return ordersItemsNamesfromOrderWithCommentA;
//    }
    
    //streams
    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
    	
    	if (orders == null) {
    		return null;
    	} else {
	    	List<String> ordersItemsNamesfromOrderWithCommentA = orders.stream()
	    			.filter(order -> order.getComments().startsWith("A"))
	    			.flatMap(order -> order.getItems().stream())
	    			.map(item -> item.getName())
	    			.distinct()
	    			.sorted()
	    			.collect(Collectors.toList());
	    			
	    	return ordersItemsNamesfromOrderWithCommentA;
    	}
    }
    
//    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
//    	
//    	for (Order order : orders) {
//    		if (order.getClientDetails().getName().startsWith("S")) {
//    			System.out.println(order.getClientDetails().getLogin().toUpperCase());
//    		}
//    	}
//    }
    
    //streams
    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
    	
    	orders.stream()
    		.filter(order -> order.getClientDetails().getName().startsWith("S"))
    		.map(order -> order.getClientDetails().getLogin().toUpperCase())
    		.forEach(System.out::println);
    }
    
//    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
//    	
//    	Map<ClientDetails, List<Order>> ordersByClientMap = new HashMap<ClientDetails, List<Order>>();
//    	
//    	List<ClientDetails> clients = new ArrayList<ClientDetails>();
//    	
//    	for (Order order : orders) {
//    		if (!clients.contains(order.getClientDetails())) {
//    			clients.add(order.getClientDetails());
//    		}
//    	}
//    	
//    	for (ClientDetails client : clients) {
//    		ordersByClientMap.put(client, new ArrayList<Order>());
//        	
//    		for (Order order: orders) {
//    			if (order.getClientDetails() == client) {
//    				ordersByClientMap.get(order.getClientDetails()).add(order);
//    			}
//        	}
//    	}
//      	
//    	return ordersByClientMap;
//    }
    
    //streams
    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
    	
    	if (orders == null) {
    		return null;
    	} else {
	    	Map<ClientDetails, List<Order>> ordersByClient = orders.stream()
	    			.collect(Collectors.groupingBy(Order::getClientDetails));
	    	
	    	return ordersByClient;
    	}
    }
    
//    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
//    	
//    	Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = new HashMap<Boolean, List<ClientDetails>>();
//    	
//    	List<ClientDetails> clients = new ArrayList<ClientDetails>();
//    	List<ClientDetails> clientsUnder18 = new ArrayList<ClientDetails>();
//    	List<ClientDetails> clientsOver18 = new ArrayList<ClientDetails>();
//    	
//    	for (Order order : orders) {
//    		if (!clients.contains(order.getClientDetails())) {
//    			clients.add(order.getClientDetails());
//    		}
//    	}
//    	
//    	for (ClientDetails client : clients) {
//    		if (client.getAge() < 19) {
//    			clientsUnder18.add(client);
//    		}
//    		else {
//    			clientsOver18.add(client);
//    		}
//    	}
//    	
//    	clientsUnderAndOver18.put(true, clientsOver18);
//    	clientsUnderAndOver18.put(false, clientsUnder18);
//    	
//    	return clientsUnderAndOver18;
//    }
//    
//}

    //streams
	public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
		
		if (orders == null) {
			return null;
		} else {
			Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = orders.stream()
					.map(order -> order.getClientDetails())
					.distinct()
					.collect(Collectors.partitioningBy(clientDetails -> clientDetails.getAge() > 17));
	
			return clientsUnderAndOver18;
		}
	}
}