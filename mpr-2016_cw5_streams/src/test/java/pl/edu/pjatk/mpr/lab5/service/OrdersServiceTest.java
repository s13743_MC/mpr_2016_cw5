package pl.edu.pjatk.mpr.lab5.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Before;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.pjatk.mpr.lab5.model.Address;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

public class OrdersServiceTest {
	
	private ClientDetails clientMikolaj;
	private ClientDetails clientMarek;
	private ClientDetails clientKowalski;
	private ClientDetails clientNowak;
	
	private Address addressOlsztyn;
	private Address addressGdansk;
	private Address addressKatowice;
	
	private OrderItem orderItem1;
	private OrderItem orderItem2;
	private OrderItem orderItem3;
	private OrderItem orderItem4;
	private OrderItem orderItem5;
	private OrderItem orderItem6;
	private OrderItem orderItem7;
	private OrderItem orderItem8;
	private OrderItem orderItem9;
	private OrderItem orderItem10;
	
	private List<OrderItem> orderItems1;
	private List<OrderItem> orderItems2;
	private List<OrderItem> orderItems3;
	private List<OrderItem> orderItems4;
	private List<OrderItem> orderItems5;
	private List<OrderItem> orderItems6;
	
	private Order order1;
	private Order order2;
	private Order order3;
	private Order order4;
	private Order order5;
	private Order order6;
	
	private List<Order> orders;
	
	@Before
	public void createTestData() {
		
		clientMikolaj = new ClientDetails(1, "mik", "Mikolaj", "Chodkowski", 28);
		clientMarek = new ClientDetails(2, "mar", "Marek", "Chodkowski", 58);
		clientKowalski = new ClientDetails(3, "kow", "Sebastian", "Kowalski", 35);
		clientNowak = new ClientDetails(4, "now", "Andrzej", "Nowak", 17);
				
		addressOlsztyn = new Address(1, "Marii Zientary-Malewskiej", "27", "1", "10-307", "Olsztyn", "Poland");
		addressGdansk = new Address(2, "Pomorska", "16a", "17", "80-333", "Gdansk", "Poland");
		addressKatowice = new Address(3, "Hutnicza", "3", "5", "30-654", "Katowice", "Poland");
		
		orderItem1 = new OrderItem(1, "Sherman", "American tank from 1942", 5000.00);
		orderItem2 = new OrderItem(2, "Leopard", "German tank", 3000.00);
		orderItem3 = new OrderItem(3, "Rosomak", "Polish military vehicle", 10000.00);
		orderItem4 = new OrderItem(4, "Jastrzab", "American Polish F-16", 300000.00);
		orderItem5 = new OrderItem(5, "Anders", "Polish prototype tank", 25000.00);
		orderItem6 = new OrderItem(6, "7TP", "Polish tank from 1939", 8000.00);
		orderItem7 = new OrderItem(7, "Bosfor", "Polish gun from 1939", 2500.00);
		orderItem8 = new OrderItem(8, "U1", "Polish rifle to destroy german tanks", 3000.00);
		orderItem9 = new OrderItem(9, "Vis", "Polish best firearm", 1500.00);
		orderItem10 = new OrderItem(10, "MP40", "German machine gun from 1941", 1000.00);
		
		orderItems1 = Arrays.asList(orderItem1, orderItem2, orderItem3, orderItem4, orderItem5,
				orderItem6, orderItem7, orderItem8, orderItem9, orderItem10);
		orderItems2 = Arrays.asList(orderItem4, orderItem5, orderItem7, orderItem8, orderItem9, orderItem10);
		orderItems3 = Arrays.asList(orderItem5, orderItem7, orderItem8, orderItem9, orderItem10);
		orderItems4 = Arrays.asList(orderItem1, orderItem2, orderItem3, orderItem4, orderItem5, orderItem6);
		orderItems5 = Arrays.asList(orderItem1, orderItem2, orderItem3, orderItem4);
		orderItems6 = Arrays.asList(orderItem4, orderItem5, orderItem6, orderItem7);
		
		order1 = new Order(1, clientMikolaj, addressGdansk, orderItems1,
				"Zamówienie nr 1, bardzo duze zamowienie");
		order2 = new Order(2, clientMarek, addressOlsztyn, orderItems2, "AZamówienie nr 2, srednie zamowienie");
		order3 = new Order(3, clientKowalski, addressKatowice, orderItems3, "AZamówienie nr 3");
		order4 = new Order(4, clientMikolaj, addressOlsztyn, orderItems4, "Zamówienie nr 4");
		order5 = new Order(5, clientMikolaj, addressOlsztyn, orderItems5, "Zamówienie nr 5");
		order6 = new Order(6, clientNowak, addressOlsztyn, orderItems6, "Zamówienie nr 6");
		
		orders = Arrays.asList(order1, order2, order3, order4, order5, order6);
	}
		
	@Test
	public void testFindOrdersWhichHaveMoreThan5OrderItems() {
		
		List<Order> expectedOrders = Arrays.asList(order1, order2, order4);
		
		List<Order> ordersMoreThan5streams = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
		
		assertEquals(expectedOrders, ordersMoreThan5streams);
	} //OK
	
	@Test
	public void testFindOldestClientAmongThoseWhoMadeOrders() {
		
		ClientDetails expectedOldestClient = clientMarek;
		
		ClientDetails oldestClient = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
		
		assertEquals(expectedOldestClient, oldestClient);
	} //OK
	
	@Test
	public void testFindOrderWithLongestComments() {
		
		Order expectedOrderWithLongestComments = order1;
		
		Order orderWithLongestComments = OrdersService.findOrderWithLongestComments(orders);
		
		assertEquals(expectedOrderWithLongestComments, orderWithLongestComments);
	} //OK
	
	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {
		
		String expectedNamesAbove18Years = clientMikolaj.getName() + " " + clientMikolaj.getSurname()
				+ ", " + clientMarek.getName() + " " + clientMarek.getSurname()
				+ ", " + clientKowalski.getName() + " " + clientKowalski.getSurname();
		
		String namesAbove18Years = OrdersService
			.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
		
		assertEquals(expectedNamesAbove18Years, namesAbove18Years);
	} //OK
	
	@Test
	public void testGetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {
		
		List<String> expectedOrdersItemsNamesfromOrderWithCommentA = Arrays.asList(orderItem5.getName(),
				 orderItem7.getName(), orderItem4.getName(), orderItem10.getName(), orderItem8.getName(),
				 orderItem9.getName());
		
		List<String> ordersItemsNamesfromOrderWithCommentA = OrdersService
				.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
		
		assertEquals(expectedOrdersItemsNamesfromOrderWithCommentA, ordersItemsNamesfromOrderWithCommentA);
	} //OK
	
	@Test
	public void testPrintCapitalizedClientsLoginsWhoHasNameStartingWithS() {
		
	    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
	    
	    OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(orders);
		
		String expectedResult = "KOW\r\n";
		
		assertEquals(expectedResult, outContent.toString());
	} //OK
	
	@Test
	public void testGroupOrdersByClient() {
		
		Map<ClientDetails, List<Order>> expectedOrdersByClient = new HashMap<>();
		
		List<Order> clientMikolajOrders = Arrays.asList(order1, order4, order5);
		List<Order> clientMikolajMarek = Arrays.asList(order2);
		List<Order> clientMikolajKowalski = Arrays.asList(order3);
		List<Order> clientMikolajNowak = Arrays.asList(order6);
		
		expectedOrdersByClient.put(clientMikolaj, clientMikolajOrders);
		expectedOrdersByClient.put(clientMarek, clientMikolajMarek);
		expectedOrdersByClient.put(clientKowalski, clientMikolajKowalski);
		expectedOrdersByClient.put(clientNowak, clientMikolajNowak);
		
		Map<ClientDetails, List<Order>> ordersByClient = OrdersService.groupOrdersByClient(orders);
		
		assertEquals(expectedOrdersByClient, ordersByClient);
	} //OK
	
	@Test
	public void testPartitionClientsByUnderAndOver18() {
		
		Map<Boolean, List<ClientDetails>> expectedClientsUnderAndOver18 = new HashMap<>();
		
		List<ClientDetails> clientsUnder18 = Arrays.asList(clientNowak);
		List<ClientDetails> clientsOver18 = Arrays.asList(clientMikolaj, clientMarek, clientKowalski);
		
		expectedClientsUnderAndOver18.put(false, clientsUnder18);
		expectedClientsUnderAndOver18.put(true, clientsOver18);
		
		Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = OrdersService
				.partitionClientsByUnderAndOver18(orders);
		
		assertEquals(expectedClientsUnderAndOver18, clientsUnderAndOver18);
	} //OK
}